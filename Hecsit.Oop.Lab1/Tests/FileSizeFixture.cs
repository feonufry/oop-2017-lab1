﻿using NUnit.Framework;

namespace Hecsit.Oop.Lab1.Tests
{
	[TestFixture]
	public class FileSizeFixture
	{
		[Test]
		public void FileSize_in_bytes_can_be_created()
		{
			var size = FileSize.FromBytes(1025);
			Assert.AreEqual(1025, size.Bytes);
		}

		[Test]
		public void FileSize_in_kilobytes_can_be_created()
		{
			var size = FileSize.FromKilobytes(2);
			Assert.AreEqual(2, size.Kilobytes);
		}

		[Test]
		public void FileSize_in_kilobytes_is_calculated()
		{
			var size = FileSize.FromBytes(3 * 1024 + 1023);
			Assert.AreEqual(3, size.Kilobytes);
		}

		[Test]
		public void Sum_of_FileSizes_gives_a_FileSize_with_sum_of_bytes()
		{
			var size1 = FileSize.FromBytes(1025);
			var size2 = FileSize.FromBytes(578);

			var actual = size1.Add(size2);
			Assert.AreEqual(1025 + 578, actual.Bytes);
		}
	}
}