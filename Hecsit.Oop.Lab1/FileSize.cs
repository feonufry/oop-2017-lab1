﻿using System.ComponentModel;

namespace Hecsit.Oop.Lab1
{
	public class FileSize
	{
		private const int BytesPerKilobyte = 1024;

		private readonly int _bytes;

		public int Bytes
		{
			get { return _bytes; }
		}

		public int Kilobytes
		{
			get { return _bytes/BytesPerKilobyte; }
		}

		private FileSize(int bytes)
		{
			_bytes = bytes;
		}

		public static FileSize FromBytes(int bytes)
		{
			return new FileSize(bytes);
		}

		public static FileSize FromKilobytes(int kilobytes)
		{
			return new FileSize(kilobytes*BytesPerKilobyte);
		}

		public FileSize Add(FileSize other)
		{
			return new FileSize(_bytes + other._bytes);
		}
	}
}